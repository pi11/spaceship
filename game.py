#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import curses
import sys
import time
from curses import KEY_RIGHT, KEY_LEFT, KEY_UP, KEY_DOWN
from random import randint


WIDTH = 80
HEIGHT = 25
BOOM_CHARS = ['.', '%', '+', '=', 'W',
              '%', '?', 'p', 'o', '0', 'O', 'O', ' ', ' ', ' ']


class GameObject:

    def __init__(self, x, y, char, attrs=None):
        self.x = x
        self.y = y
        self.old_x = x
        self.old_y = y
        self.char = char
        self.attrs = attrs
        self.status = 0

    def x(self):
        return self.x

    def y(self):
        return self.y

    def clear(self):
        if self.old_x < WIDTH and self.old_x > 1 and \
           self.old_y < HEIGHT and self.old_y > 1:
            win.addch(self.old_y, self.old_x, ' ')
        if self.x < WIDTH and self.x > 1 and \
           self.y < HEIGHT and self.y > 1:
            win.addch(self.y, self.x, ' ')

    def move(self, x, y):
        self.clear()
        if x > WIDTH - 1 or \
           x < 2 or \
           y > HEIGHT - 1 or \
           y < 2:
            return  # Do not move over the win

        self.old_x = self.x  # store old coordinates
        self.old_y = self.y
        self.x = x
        self.y = y

    def draw(self):
        self.clear()
        # print self.y, self.x, self.attrs, "\n"
        if self.attrs:
            win.addch(self.y, self.x, self.char, self.attrs)
        else:
            win.addch(self.y, self.x, self.char,)

    def set_char(self, char):
        self.char = char


class Ship(GameObject):
    pass


class Enemy(GameObject):

    def do_move(self):
        try:
            any([self.in_tic, self.tic])
        except AttributeError:
            self.in_tic = 30 - randint(0, 20)
            self.tic = self.in_tic

        self.tic -= 1
        if self.tic < 1:
            self.tic = self.in_tic
            self.move(self.x - 1, self.y)


class Missile(GameObject):
    pass


class EnemyMissile(GameObject):
    pass


class Boom(GameObject):

    def draw(self):
        self.clear()
        if self.attrs:
            win.addch(self.y, self.x, self.char, self.attrs)
        else:
            win.addch(self.y, self.x, self.char,)
        self.status += 1
        self.set_char(BOOM_CHARS[self.status])


class Star(GameObject):
    pass


class Game:

    def __init__(self):

        self.is_dead = False
        self.is_finished = False
        self.is_win = False
        self.is_win_process = False

        self.key = None
        self.last_key = None
        self.is_key_pressed = False
        self.key_m_tic = 70
        self.key_tic = self.key_m_tic
        self.start_moving = False
        self.base_pos = WIDTH - 35
        self.score = 0
        self.stars = []
        self.missiles = []
        self.enemies = []
        self.enemies_missiles = []

        self.booms = []
        self.stars_tic = 30
        self.enemies_tic = 15
        self.stars_m_tic = 30
        self.enemies_m_tic = 15
        self.base_tic = 10

        self.lives = 5
        self.level = 1
        self.total_missiles = 1

        # ship init
        self.ship = Ship(50, 10, '>')

        # stars init
        for j in range(0, 40):
            star = Star(
                y=randint(2, HEIGHT - 2), x=randint(2, WIDTH - 2), char='.')
            self.stars.append(star)

        # enemies init
        for j in range(0, 3):
            enemy = Enemy(y=
                          randint(2, HEIGHT - 2), x=WIDTH - 1, char='*', attrs=curses.color_pair(1))

            self.enemies.append(enemy)

    def draw_star(self, x, y):
        base = """
                     _____
                 ,-~"     "~-.
               ,^ ___         ^.
              / .^   ^.         \
             |  l  o  !          |
             l_ `.___.'        _,[
             |^~"-----------""~ ^|
             !                   !
              \                 /
               ^.             .^
                 "-.._____.,-"

        """.split("\n")
        k = 0
        for l in base:
            k += 1
            win.addstr(y + k, x, l)

    def do_win(self):

        for e in self.enemies:
            e.clear()
        for e in self.enemies_missiles:
            e.clear()
        for e in self.stars:
            e.clear()

        self.enemies = []
        self.enemies_missiles = []
        self.stars = []

        win.addch(self.ship.y, self.ship.x, 'X')
        if self.base_pos > 20:
            self.base_tic -= 1
            if self.base_tic < 1:
                self.base_tic = 10
                self.base_pos -= 1

        self.is_win = True

        self.draw_star(self.base_pos, HEIGHT - 15)

        win.addstr(5, 15,   '          YOU WIN!    ',
                   curses.color_pair(2))

        with open('data/end_text.txt') as f:
            end_text = f.read()
        k = 0
        for l in end_text.split("\n"):
            k += 1
            win.addstr(6 + k, 15, l)

    def die(self):
        for e in self.enemies:
            e.clear()
        for e in self.enemies_missiles:
            e.clear()
        self.enemies = []
        self.enemies_missiles = []

        win.addstr(
            HEIGHT // 2, 35,   '          GAME OVER    ', curses.color_pair(2))
        win.addstr(
            HEIGHT // 2 + 1, 35, '    Press Esc to continue    ', curses.A_STANDOUT)
        self.is_dead = True

    def hit(self):
        self.lives -= 1
        if self.lives < 1:
            self.die()

    def fire(self):
        if len(self.missiles) < self.total_missiles:
            missile = Missile(self.ship.x + 1, self.ship.y, '-')
            self.missiles.append(missile)

    def draw(self):

        win.border(0)
        win.addstr(0, 2, ' Score : %s ' % self.score)
                   # Printing 'Score' and
        win.addstr(0, 20, ' Level : %s | Missiles : %s ' %
                   (self.level, self.total_missiles))
        win.addstr(0, 50, ' Lives : %s ' % ('>' * self.lives))
        win.timeout(28 - (self.level + 2))

        for s in self.stars:
            s.draw()
        for m in self.missiles:
            m.draw()
        for m in self.enemies_missiles:
            m.draw()
        for e in self.enemies:
            e.draw()
        for b in self.booms:
            b.draw()

        if self.is_win_process:
            self.do_win()

        self.ship.draw()

    def calc(self):
        """Calc positions lol"""
        if self.is_dead or self.is_win:
            return True

        # calc level
        levels = [5, 10, 20, 35, 55, 70, 95, 120, 150, 200]
        k = 0
        for l in levels:
            k += 1
            if self.score > l:
                self.level = k

        if self.level > 9:
            self.is_win_process = True

        self.total_missiles = self.level + 1
        if self.total_missiles > 5:
            self.total_missiles = 6
        self.enemies_tic -= 1

        # enemy missiles
        for em in self.enemies_missiles:
            em.move(em.x - 1, em.y)

        while len(self.enemies) < self.level * 2 + 3:
            enemy = Enemy(
                y=randint(2, HEIGHT - 2), x=WIDTH - 1, char='*', attrs=curses.color_pair(1))
            self.enemies.append(enemy)

        for enemy in self.enemies:
            enemy.do_move()  # (enemy.x - 1, enemy.y)
            if enemy.x < 3:
                self.enemies.remove(enemy)
                enemy.clear()

        if self.enemies_tic == 0:
            for enemy in self.enemies:
                self.enemies_tic = self.enemies_m_tic - self.level

                if randint(0, 45 - self.level) == 1:
                    enemymissile = EnemyMissile(
                        x=enemy.x - 1, y=enemy.y, char="~", attrs=curses.color_pair(2))
                    self.enemies_missiles.append(enemymissile)

                m_r = abs(self.ship.x - enemy.x)
                if randint(0, m_r) == 0:
                    if self.ship.y < enemy.y:
                        enemy.move(enemy.x, enemy.y - 1)
                    elif self.ship.y > enemy.y:
                        enemy.move(enemy.x, enemy.y + 1)

        # calc missiles:
        for missile in self.missiles:
            missile.move(missile.x + 1, missile.y)
            if missile.x > WIDTH - 2:
                missile.clear()
                self.missiles.remove(missile)  # out of screen
                missile = None
        # calc stars
        self.stars_tic -= 1
        if self.stars_tic == 0:
            self.stars_tic = self.stars_m_tic
            for star in self.stars:
                star.move(star.x - 1, star.y)
                if star.x < 3:
                    star.clear()
                    star.move(y=randint(2, HEIGHT - 2), x=WIDTH - 2)
                              # move star to begin of screen

        # CHeck for collision
        for em in self.enemies_missiles:

            if em.x < 3:
                em.clear()
                self.enemies_missiles.remove(em)
                em = None
            for m in self.missiles:
                if em:
                    if (em.x == m.x and em.y == m.y) or \
                            (em.x + 1 == m.x and em.y == m.y):
                        boom = Boom(
                            x=m.x, y=m.y, char=BOOM_CHARS[0], attrs=curses.color_pair(2))
                        self.booms.append(boom)
                        self.missiles.remove(m)
                        self.enemies_missiles.remove(em)
                        m.clear()
                        em.clear()

            if em:
                if em.x == self.ship.x and em.y == self.ship.y:
                    self.hit()
                    try:
                        self.enemies_missiles.remove(em)
                    except ValueError:
                        pass
                    em.clear()
                    boom = Boom(
                        x=em.x, y=em.y, char=BOOM_CHARS[0], attrs=curses.color_pair(2))
                    self.booms.append(boom)

        for enemy in self.enemies:

            for missile in self.missiles:
                if (missile.x == enemy.x and missile.y == enemy.y) or \
                   (missile.x + 1 == enemy.x and missile.y == enemy.y):
                    self.score += 1
                    boom = Boom(
                        x=missile.x, y=missile.y, char=BOOM_CHARS[0], attrs=curses.color_pair(2))
                    self.booms.append(boom)

                    self.enemies.remove(enemy)
                    enemy.clear()
                    # enemy = None

                    self.missiles.remove(missile)
                    missile.clear()
                    # missile = None

            if enemy.x == self.ship.x and enemy.y == self.ship.y:
                self.hit()
                try:
                    self.enemies.remove(enemy)
                except ValueError:
                    pass
                enemy.clear()
                boom = Boom(
                    x=enemy.x, y=enemy.y, char=BOOM_CHARS[0], attrs=curses.color_pair(2))
                self.booms.append(boom)

        # Booooms
        for b in self.booms:
            b.status += 1
            if b.status > 12:
                b.clear()
                self.booms.remove(b)
                b = None

    def proccess_key(self, key):

        # win.addstr(1, 2, "K:%s : %s (%s), %s" %
        #           (str(key), str(self.last_key), self.key_tic, self.start_moving))

        if key == ord(' '):  # fire
            self.fire()

        if key == ord("w"):
            self.do_win()

        self.key = key
        if key in [KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, 27]:
            if self.last_key != key:
                self.start_moving = True
                self.last_key = key

        if self.start_moving:
            if key == -1:
                if self.key_tic % 7 == 0:
                    key = self.last_key

            self.key_tic -= 1
            if self.key_tic < 1:
                self.start_moving = False
                self.key_tic = self.key_m_tic
                self.last_key = None

        if key == KEY_UP and self.ship.y > 1:
            self.ship.move(x=self.ship.x, y=self.ship.y - 1)
        if key == KEY_DOWN and self.ship.y + 2 < HEIGHT:
            self.ship.move(x=self.ship.x, y=self.ship.y + 1)
        if key == KEY_LEFT and self.ship.x > 1:
            self.ship.move(x=self.ship.x - 1, y=self.ship.y)
        if key == KEY_RIGHT and self.ship.x < WIDTH - 2:
            self.ship.move(x=self.ship.x + 1, y=self.ship.y)


# Lets go

j = 5
with open('data/title.txt') as f:
    print (f.read())
    time.sleep(2)

input("Press Enter to continue...")

screen = curses.initscr()
win = curses.newwin(HEIGHT, WIDTH, 0, 0)
win.keypad(1)
curses.noecho()
curses.curs_set(0)
curses.start_color()
curses.init_pair(1, curses.COLOR_YELLOW, curses.COLOR_BLACK)
curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
curses.init_pair(3, curses.COLOR_MAGENTA, curses.COLOR_BLACK)

win.border(0)
curses.cbreak()
win.nodelay(1)


move_counter = 1000
game = Game()
key = None
finished = False

while key != 27 and not finished:  # 27 - esc
    key = win.getch()
    game.proccess_key(key)
    finished = game.calc()
    game.draw()

if finished:
    while key != 27:  # 27 - esc
        key = win.getch()

curses.endwin()

print("Bye")
